﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Text.RegularExpressions;
using ICSharpCode.SharpZipLib.Zip;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Web.Security;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;



public partial class StateProfile : System.Web.UI.Page
{
    int StateID;
    string StateName;
    int SchoolY;
    public string StateNameMap;
    string LiaisonDirectory;
    string SYear;

    string sqlCon = ConfigurationManager.ConnectionStrings["NCHEConnectionString"].ConnectionString;
    protected void Page_Load(object sender, EventArgs e)
    {
       
       
        if (!IsPostBack)
        {
            if (Page.Request.QueryString["StateID"] != null)
            {
                StateID = Convert.ToInt32(Page.Request.QueryString["StateID"]);
            }
            GetStateName(StateID);
            loadData(StateName);
           
        }

        GetStateName(StateID);
        DisplayStateData(StateName);
        DynamicGridView(StateName);

        Repeater3.DataSource = PercBelowPoverty(StateName);
        Repeater3.DataBind();

        Repeater5.DataSource = TotalNoOFLEA(StateName);
        Repeater5.DataBind();

        Repeater6.DataSource = TotalStudentEnrolled(StateName);
        Repeater6.DataBind();

        Repeater4.DataSource = StatedataNotes(StateName);
        Repeater4.DataBind();


        Repeater2.DataSource = CaseStudyProjectData(StateName);
        Repeater2.DataBind();

        Repeater1.DataSource = CaseStudyProjectData(StateName);
        Repeater1.DataBind();

        DataSet dsCheck = new DataSet();
        dsCheck = CaseStudyProjectData11(StateName);
        Repeater11.DataSource = dsCheck;
        Repeater11.DataBind();

        Repeater7.DataSource = PercBelowPoverty(StateName);
        Repeater7.DataBind();


        Repeater8.DataSource = PercBelowPoverty(StateName);
        Repeater8.DataBind();


        Repeater9.DataSource = PercBelowPoverty(StateName);
        Repeater9.DataBind();

        //Repeater10.DataSource = PercBelowPoverty(StateName);
        //Repeater10.DataBind();

        RLiason.DataSource = CaseStudyProjectData(StateName);
        RLiason.DataBind();


        Page.MaintainScrollPositionOnPostBack = true;

    }

    private DataSet CaseStudyProjectData11(string StateName)
    {
        SqlConnection con = new SqlConnection(sqlCon);
        DataSet ds = new DataSet();
        con.Open();
        string sql = "SELECT Prefix2,FirstName2,LastName2,[Combined name2],Address12,Address22,Organization22,Organization23,City2 ,State2 ,Zip2 , [Phone Number2],[Fax Number2],Email2 FROM StateContactCombined where StateName=@StateName";
        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.CommandType = CommandType.Text;

        cmd.Parameters.Add("@StateName", StateName);

        try
        {
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        string value = ds.Tables[0].Rows[0][0].ToString();

        if (string.IsNullOrEmpty(value))
        {
            divContact.Visible = false;
        }
        else
        {
            divContact.Visible = true;
        }
        return ds;

    }



    private object TotalStudentEnrolled(string StateName)
    {

        //ReportData rep1 = new ReportData();
        //DataSet ds = new DataSet();
        //DataTable dt = new DataTable();
        //ds = rep1.Get_FastFact(StateName);
        //dt = ds.Tables[0];
        //return dt;

        SqlConnection con = new SqlConnection(sqlCon);
        con.Open();

        string sql = "SELECT MEMBER, CAST(Perc_Hmls_Enr_1213*100 as decimal(15,1)) as Perc_Hmls_Enr_1213  from Master_SEA where StateName=@StateName";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@StateName", StateName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;
    }

    private object TotalNoOFLEA(string StateName)
    {
        //string SchoolYear = "2013-2014";
        //ReportData rep1 = new ReportData();
        //DataSet ds = new DataSet();
        //DataTable dt = new DataTable();
        //ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEACount");
        //dt = ds.Tables[0];
        //return dt;

        SqlConnection con = new SqlConnection(sqlCon);
        con.Open();

        string sql = "SELECT LEA_Tot,LEA_Grant from Master_SEA where StateName=@StateName";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@StateName", StateName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;
    }

    private object PercBelowPoverty(string StateName)
    {
        SqlConnection con = new SqlConnection(sqlCon);
        con.Open();

        string sql = "SELECT StateName,Perc_below_pov * 100 As Perc_below_pov, Perc_kids_pov * 100 As Perc_kids_pov from Master_SEA where StateName=@StateName";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@StateName", StateName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;
    }

    private void loadData(string StateName)
    {
        // Display state notes:
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var qryS = from e in db.SchoolYears
                       where e.SYearID >= 2 && e.SYearID<=9
                       select e;
            ddlConsolidatedSPR.DataSource = qryS;
            ddlConsolidatedSPR.DataTextField = "SchoolYearConsolidated";
            ddlConsolidatedSPR.DataValueField = "SYearID";
            ddlConsolidatedSPR.DataBind();
            ddlConsolidatedSPR.Items.Insert(0, "");

            var qrr = from f in db.StateContactCombineds
                      where f.StateName == StateName
                      select f;
            foreach (var res in qrr)
            {
                if (String.IsNullOrEmpty(res.LiaisonDirectory.ToString()))
                {
                    divhide.Visible = false;
                }
                else
                {
                    divhide.Visible = true;
                }
            }
        }

    }

    private object StatedataNotes(string StateName)
    {

        SqlConnection con = new SqlConnection(sqlCon);
        con.Open();

        string sql = "SELECT * FROM StateContactCombined where StateName=@StateName";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@StateName", StateName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        if (string.IsNullOrEmpty(dt.Rows[0]["Comment 1"].ToString()) && string.IsNullOrEmpty(dt.Rows[0]["Comment 2"].ToString()))
        {
            lblAddnotes.Text = "Additional state data notes is not available for this state.";
        }


        return dt;

    }


    private object countLEAState(string StateName)
    {
        string SchoolYear = "";
        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        DataTable dt = new DataTable();
        ds = rep1.Get_AllSEAEnrolled_search(SchoolYear, StateName, "spLEA_Enrolled_LEA");
        dt = ds.Tables[0];
        return dt;

    }

    private void GetStateName(int StateID)
    {
        object obValue;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            var oAU = from ans in db.States
                      where ans.StateID == StateID
                      select ans;

            foreach (var gSn in oAU)
            {
                StateName = gSn.StateFullName.ToString();
                StateNameMap = StateName;
            }
        }
    }

    private void DynamicGridView(string StateName)
    {
        string SchoolYear = "";
        ReportData rep1 = new ReportData();
        DataSet ds = new DataSet();
        ds = rep1.get_SubgroupOfHomeless(StateName);

        DataTable dt = new DataTable();
        dt = ds.Tables[0];
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    private void DisplayStateData(string StateName)
    {

    }
    protected override void LoadViewState(object savedState)
    {
        base.LoadViewState(savedState);

        if (((this.ViewState["StateID"] != null)))
        {
            StateID = Convert.ToInt32(this.ViewState["StateID"]);
        }
    }
    protected override object SaveViewState()
    {
        this.ViewState["StateID"] = StateID;
        return (base.SaveViewState());
    }
    protected void btndownload_Click(object sender, EventArgs e)
    {
        if (ddlConsolidatedSPR.SelectedIndex != -1 && ddlConsolidatedSPR.SelectedItem.Text != "")
        {
            SchoolY = Convert.ToInt32(ddlConsolidatedSPR.SelectedValue);
        }
        else
        {
            SchoolY = 0;
        }
        //Response.Write("<script>window.open('StateProfileCSPRpdf.aspx?StateID="+StateID+ "&SYearID=" + SchoolY + "', '_blank');</script>");
         displayDocument(SchoolY);
    }

    private void displayDocument(int SchoolY)
    {
       // LiteralMessage.Text = "";
        object objVal = null;
        using (DataClassesDataContext db = new DataClassesDataContext())
        {
            StateProfileCSPR oCase1 = (from pg in db.StateProfileCSPRs where pg.SYearID_fk == SchoolY && pg.StateID_fk == StateID select pg).FirstOrDefault();

            if (oCase1!=null)
            {
                LiteralMessage.Text = "";
            }
            else
            {
                LiteralMessage.Text = "This file does not exist.";
            }
            var oApps = from pg in db.StateProfileCSPRs
                        where pg.SYearID_fk == SchoolY && pg.StateID_fk == StateID
                        select pg;         
            foreach (var oApp in oApps)
            {
                objVal = oApp.FileName;
                if (objVal != null)
                {
                  //  LiteralMessage.Text = "";
                    displayDocument01(objVal.ToString());
                }                
            }          
        }       
    }

    private void displayDocument01(string strFileName)
    {
        if (!string.IsNullOrEmpty(strFileName))
        {
            NameValueCollection appSettings = System.Configuration.ConfigurationManager.AppSettings;           
            string sTempFileName = null;
            sTempFileName = ConfigurationManager.AppSettings["SourceFilePath"].ToString();
            System.IO.FileInfo file = new System.IO.FileInfo(sTempFileName + "/" + strFileName);
            //-- if the file exists on the server
            //set appropriate headers
            if (file.Exists)
            {
                LiteralMessage.Text = "";
                Response.Clear();
                Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Length", file.Length.ToString());
                string sExtn;
                sExtn = file.Extension;
                switch (sExtn)
                {
                    case ".pdf":
                        Response.ContentType = "application/pdf";
                        break;
                    case ".xls":
                        Response.ContentType = "application/excel";
                        break;
                    case ".xlsx":
                        Response.ContentType = "application/excel";
                        break;
                    case ".doc":
                        Response.ContentType = "application/msword";
                        break;
                    case "docx":
                        Response.ContentType = "application/msword";
                        break;
                    default:
                        Response.ContentType = "application/octet-stream";
                        break;
                }
                LiteralMessage.Text = "";
                Response.WriteFile(file.FullName);
                Response.End();
                //if file does not exist
            }
            else
            {
           // LiteralMessage.Text = "This file does not exist.";
            }
            //nothing in the URL as HTTP GET
        }
        else
        {
         //  LiteralMessage.Text = "This file does not exist.";
        }
    }
    //protected void btnpdfReport_Click(object sender, EventArgs e)
    //{

    //}

    private object CaseStudyProjectData(string StateName)
    {
        SqlConnection con = new SqlConnection(sqlCon);

        con.Open();

        string sql = "SELECT * FROM StateContactCombined where StateName=@StateName";

        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.Parameters.Add("@StateName", StateName);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataTable dt = new DataTable();
        da.Fill(dt);
        con.Close();
        return dt;


    }
    protected void btnSpreadsheets_Click(object sender, EventArgs e)
    {      
     // Response.Write("<script>window.open('http://nchespp.serve.org/download/412?state_id=7','_blank');</script>");
     ClientScript.RegisterStartupScript(this.GetType(), "PopupWindow", "<script language='javascript'>window.open('http://nchespp.serve.org/download/412?state_id=7','Title')</script>");
    }

    protected void ddlConsolidatedSPR_SelectedIndexChanged(object sender, EventArgs e)
    {
         SYear = ddlConsolidatedSPR.SelectedItem.Text.ToString();
         LiteralMessage.Text = "";
        
    }
    protected void btnConvertToPDF_Click(object sender, EventArgs e)
    {
        //Response.ContentType = "application/pdf";
        //Response.AddHeader("content-disposition", "attachment;filename=Stateprofile.pdf");
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //StringWriter sw = new StringWriter();
        //HtmlTextWriter hw = new HtmlTextWriter(sw);

        //this.Page.RenderControl(hw);

        //StringReader sr1 = new StringReader(sw.ToString());

        //string swString = sw.ToString();

        //int i = swString.IndexOf("<article>");

        //string holdString = swString.Substring(i);

        //StringReader sr2 = new StringReader(holdString);

        //Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
        //PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        //pdfDoc.Open();
        //htmlparser.Parse(sr2);
        //pdfDoc.Close();
        //Response.Write(pdfDoc);
        //Response.End();        

    }
}