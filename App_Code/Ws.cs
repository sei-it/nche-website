﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Ws
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class Ws : System.Web.Services.WebService {
    string StName;
    string SYear;
    public Ws () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    [WebMethod]
    public string StateName(string StateName)
    {
        return StateName.ToString();
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }
    public class SEAEnrolled
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    [WebMethod]
    public List<SEAEnrolled> SEAEnrolled_Analysis(string StateName, string SchoolYear)   
    {
        List<SEAEnrolled> SEAEnrolledinfo = new List<SEAEnrolled>();
        DataSet ds = new DataSet();
        using (SqlConnection con = new SqlConnection("Data Source=192.168.16.219;User Id=AppUserNHCEExtranet;Password=Tbdvv@537!;DataBase=NCHE"))
        {
          
            StName = StateName.ToString();
            SYear = SchoolYear.ToString();

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "select Enr_SEA_StudentCount As TotalCount ,Enr_SEA_Category from SEAEnrolled_SY11_14 where StateName=@StateName and SchoolYear=@SchoolYear and Enr_SEA_CategoryAbbr in ('D','HM','S','U')";
                cmd.Connection = con;
                cmd.Parameters.Add("@StateName", StName);
                cmd.Parameters.Add("@SchoolYear", SYear);
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds, "SEAEnrolled_Analysis");
                }
            }
        }
        if (ds != null)
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables["SEAEnrolled_Analysis"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["SEAEnrolled_Analysis"].Rows)
                    {
                        SEAEnrolledinfo.Add(new SEAEnrolled
                        {
                            Name = dr["Enr_SEA_Category"].ToString(),
                            Value = Convert.ToInt32(dr["TotalCount"])
                        });
                    }
                }
            }
        }
        return SEAEnrolledinfo;
    }
    
}
