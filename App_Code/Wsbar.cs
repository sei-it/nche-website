﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for Wsbar
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class Wsbar : System.Web.Services.WebService {

    string StName;
    public Wsbar () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld() {
        return "Hello World";
    }
    public class SEAEnrolled
    {
        public string Name { get; set; }
        public int Value { get; set; }
    }

    [WebMethod]
    public List<SEAEnrolled> SEAEnrolled_Analysis(string StateName)
    {
        List<SEAEnrolled> SEAEnrolledinfo = new List<SEAEnrolled>();
        DataSet ds = new DataSet();
        using (SqlConnection con = new SqlConnection("Data Source=192.168.16.219;User Id=AppUserNHCEExtranet;Password=Tbdvv@537!;DataBase=NCHE"))
        {
                StName = StateName.ToString();         
                //SqlCommand cmd = new SqlCommand("SYEnrolled_BarChartHomeless", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@StateName", StName); 
                //try
                //{
                //    con.Open();
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    da.Fill(ds, "SEAEnrolled_Analysis");
                //    con.Close();
                //}
                //catch (Exception ex)
                //{
                //    Console.Write(ex.ToString());
                //}

            using (SqlCommand cmd = new SqlCommand())
            {
                cmd.CommandText = "SELECT StateName, TotalEnrHomeless, ASMT.Score FROM Master_SEA UNPIVOT( Score FOR TotalEnrHomeless IN (ENR_1112_PKto12, ENR_1213_PKto12, ENR_1314_Pkto12)) AS ASMT where StateName=@StateName";
                cmd.Connection = con;
                cmd.Parameters.Add("@StateName", StName);               
                using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                {
                    da.Fill(ds, "SEAEnrolled_Analysis");
                }
            }
               
            
        }
        if (ds != null)
        {
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables["SEAEnrolled_Analysis"].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables["SEAEnrolled_Analysis"].Rows)
                    {
                        SEAEnrolledinfo.Add(new SEAEnrolled
                        {
                            Name = dr["TotalEnrHomeless"].ToString(),
                            Value = Convert.ToInt32(dr["Score"])
                        });
                    }
                }
            }
        }
        return SEAEnrolledinfo;
    }
    
    
}
