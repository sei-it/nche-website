﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// Summary description for HighchartDemo
/// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]

    public class HighchartDemo : System.Web.Services.WebService
    {

        public class SEAEnrolled
        {
            public string Name { get; set; }
            public int Value { get; set; }
        }

        [WebMethod]
        public List<SEAEnrolled> SEAEnrolled_Analysis()
        {
            List<SEAEnrolled> SEAEnrolledinfo = new List<SEAEnrolled>();
            DataSet ds = new DataSet();
            using (SqlConnection con = new SqlConnection("Data Source=192.168.16.219;User Id=AppUserNHCEExtranet;Password=Tbdvv@537!;DataBase=NCHE"))
            {

                string SchoolYear = "2011-2012";
                string StateName = "Alabama";

                //ReportData rep1 = new ReportData();
                //ds = rep1.Get_PieChart(SchoolYear, StateName, "SYEnrolled_PieChart");


                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Enr_SEA_StudentCount As TotalCount ,Enr_SEA_CategoryAbbr from SEAEnrolled_SY11_14 where StateName='Alabama' and SchoolYear='2011-2012' and Enr_SEA_CategoryAbbr in ('D','HM','S','U')";
                    cmd.Connection = con;
                    using (SqlDataAdapter da = new SqlDataAdapter(cmd))
                    {
                        da.Fill(ds, "SEAEnrolled_Analysis");
                    }
                }



                //SqlCommand cmd = new SqlCommand("SYEnrolled_PieChart", con);
                //cmd.CommandType = CommandType.StoredProcedure;
                //cmd.Parameters.Add("@StateName", StateName);
                //cmd.Parameters.Add("@SchoolYear", SchoolYear);
                //try
                //{
                //    con.Open();
                //    SqlDataAdapter da = new SqlDataAdapter(cmd);
                //    da.Fill(ds);
                //    con.Close();
                //}
                //catch (Exception ex)
                //{
                //    Console.Write(ex.ToString());
                //}
            }
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables["SEAEnrolled_Analysis"].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables["SEAEnrolled_Analysis"].Rows)
                        {
                            SEAEnrolledinfo.Add(new SEAEnrolled { Name = dr["Enr_SEA_CategoryAbbr"].ToString(),
                                                                  Value = Convert.ToInt32(dr["TotalCount"])
                            });

                        }
                    }
                }
            }
            return SEAEnrolledinfo;
        }
    }
    

