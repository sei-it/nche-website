﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Configuration;
using System.Web.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Text;

/// <summary>
/// Summary description for ReportData
/// </summary>
public class ReportData
{
    string connString = ConfigurationManager.ConnectionStrings["NCHEConnectionString"].ToString();
	public ReportData()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public DataSet Get_AllSEAEnrolled_search(string SchoolYear, string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllSEAEnrolled(string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_AllFastFact(string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);        
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    internal DataSet Get_PieChart(string SchoolYear, string StateName, string p)
    {
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(p, con);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@StateName", StateName);
        cmd.Parameters.Add("@SchoolYear", SchoolYear);
        
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet Get_FastFact(string StateName)
    {
        string sql = "select LEA_Tot,LEA_Grant,MEMBER,ENR_1112_PKto12 ,ENR_1213_PKto12,ENR_1314_Pkto12,Perc_Hmls_Enr_1213*100 As Perc_Hmls_Enr_1213,Perc_below_pov * 100 as Perc_below_pov,Perc_kids_pov * 100 As Perc_kids_pov from Master_SEA	where StateName=@StateName";
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@StateName", StateName);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }

    public DataSet get_SubgroupOfHomeless(string StateName)
    {
        string sql = "select * from vw_SubgroupOfHomeless where StateName=@StateName";
        DataSet ds = new DataSet();
        SqlConnection con = new SqlConnection(connString);
        SqlCommand cmd = new SqlCommand(sql, con);
        cmd.CommandType = CommandType.Text;
        cmd.Parameters.Add("@StateName", StateName);
        try
        {
            con.Open();
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            da.Fill(ds);
            con.Close();
        }
        catch (Exception ex)
        {
            Console.Write(ex.ToString());
        }
        return ds;
    }
}