var simplemaps_usmap_mapdata = {

	main_settings:{
	    //General settings
       
		width: '700', //or 'responsive'
		background_color: '#FFFFFF',	
		background_transparent: 'no',
		border_color: '#ffffff',
		popups: 'off', //on_click, on_hover, or detect
	
		//State defaults
		state_description:   '',
		//state_color: '#366DED',
	    //state_hover_color: '#000',
		state_color: '#999900',
		state_hover_color: '#333333',
		state_url: 'http://simplemaps.com',
		border_size: 1.5,		
		all_states_inactive: 'no',
		all_states_zoomable: 'no',		
		
		//Location defaults
		location_description:  'Location description',
		location_color: '#FF0067',
		location_opacity: .8,
		location_hover_opacity: 1,
		location_url: '',
		location_size: 25,
		location_type: 'square', // circle, square, image
		location_image_source: 'frog.png', //name of image in the map_images folder		
		location_border_color: '#FFFFFF',
		location_border: 2,
		location_hover_border: 2.5,				
		all_locations_inactive: 'no',
		all_locations_hidden: 'no',
		
		//Labels
		label_color: '#d5ddec',	
		label_hover_color: '#d5ddec',		
		label_size: 17,
		label_font: 'Arial',
		hide_labels: 'no',
		hide_eastern_labels: 'no',
		
		//Zoom settings
		zoom: 'yes', //use default regions
		back_image: 'no',   //Use image instead of arrow for back zoom				
		arrow_color: '#3B729F',
		arrow_color_border: '#88A4BC',
		initial_back: 'no', //Show back button when zoomed out and do this JavaScript upon click		
		initial_zoom: -1,  //-1 is zoomed out, 0 is for the first continent etc	
		initial_zoom_solo: 'no', //hide adjacent states when starting map zoomed in
		region_opacity: 1,
		region_hover_opacity: .6,
		zoom_out_incrementally: 'yes',  // if no, map will zoom all the way out on click
		zoom_percentage: .99,
		zoom_time: .5, //time to zoom between regions in seconds
		
		//Popup settings
		popup_color: 'white',
		popup_opacity: .9,
		popup_shadow: 0,
		popup_corners: 0,
		popup_font: '10px/1.5 Verdana, Arial, Helvetica, sans-serif',
		popup_nocss: 'yes', //use your own css	
		
		//Advanced settings
		div: 'map',
		auto_load: 'yes',		
		url_new_tab: 'no', 
		images_directory: 'default', //e.g. 'map_images/'
		fade_time:  .1, //time to fade out		
		link_text: ''  //Text mobile browsers will see for links	
		
	},

	state_specific:{	
		"HI": {
			name: 'Hawaii',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=15">State Profile</a></p>',
		    color: 'default',
		    hover_color: 'default',
		    label_color: '#999900',
			url: 'StateProfile.aspx?StateID=15'
		},

		//"AK": {
		//	name: 'Alaska',
		//	//description: 'Project Downloads (2)<br />Manual Downloads (1)',
		//    //description: '<p><a class="pre" href="StateProfile.aspx?StateID=1">State Profile</a></p>',    
		//	description: '<p><a class="pre" href="StateProfile.aspx?StateID=1">State Profile</a></p>',
		//	color: '#3A17B1',
		//	hover_color: 'default',
		//	 url: 'StateProfile.aspx?StateID=1'
	    //	},

		"AK": {
		    name: 'Alaska',
		    description: 'default',
		   // description: '<p><a class="pre" href="StateProfile.aspx?StateID=1">State Profile</a></p>',
		    color: 'default',
		    hover_color: 'default',
		    url: 'StateProfile.aspx?StateID=1',
		    inactive: 'no'

		},

		"FL": {
			name: 'Florida',
		   description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=11">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=11',
			inactive: 'no'
			},
		"NH": {
			name: 'New Hampshire',
		    //description: 'default',
			description: '<p><a class="pre" href="StateProfile.aspx?StateID=36">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=36'
			},
		"VT": {
			name: 'Vermont',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=54">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=54'
			},
		"ME": {
			name: 'Maine',
		   description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=25">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=25'
	
			},
		"RI": {
			name: 'Rhode Island',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=47">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=47'
			},
		"NY": {
			name: 'New York',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=40">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=40'	
		},
		"PA": {
			name: 'Pennsylvania',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=44">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=44'			
			},
		"NJ": {
			name: 'New Jersey',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=37">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=37'
			},
		"DE": {
			name: 'Delaware',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=10">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=10'
			},
		"MD": {
			name: 'Maryland',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=24">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=24'
						
			},
		"VA": {
			name: 'Virginia',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=53">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=53'
			},
		"WV": {
			name: 'West Virginia',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=57">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=57'
			},
		"OH": {
			name: 'Ohio',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=41">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			url: 'StateProfile.aspx?StateID=41'
			},
		"IN": {
			name: 'Indiana',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=19">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=19'
			},
		"IL": {
			name: 'Illinois',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=18">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=18'
			
			},
		"CT": {
			name: 'Connecticut',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=8">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=8'
			},
		"WI": {
			name: 'Wisconsin',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=58">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=58'
			},
		"NC": {
			name: 'North Carolina',
		    description: 'default',
		//	description: '<p><a class="pre" href="StateProfile.aspx?StateID=33">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=33'
			},
		"DC": {
			name: 'District of Columbia',
		    //description: 'default',
			description: '<p><a class="pre" href="StateProfile.aspx?StateID=9">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=9'
		},
		"MA": {
			name: 'Massachusetts',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=23">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=23'
				
			},
		"TN": {
			name: 'Tennessee',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=50">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=50'
			},
		"AR": {
			name: 'Arkansas',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=3">State Profile</a></p>',

			color:  'default',
            //
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=3'
			},
		"MO": {
			name: 'Missouri',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=29">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=29'
			},
		"GA": {
			name: 'Georgia',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=13">State Profile</a></p>',
		    color: 'default',           
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=13'
			},
		"SC": {
			name: 'South Carolina',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=48">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=48'
			},
		"KY": {
			name: 'Kentucky',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=21">State Profile</a></p>',

			color:  'default',
			zoomable: 'no',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=21'
			},
		"AL": {
			name: 'Alabama',
		    //description: 'default',
			description: '<p><a class="pre" href="StateProfile.aspx?StateID=2">State Profile</a></p>',
			color:  'default',
            //
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=2'
					
			},
		"LA": {
			name: 'Louisiana',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=22">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=22'
			},
		"MS": {
			name: 'Mississippi',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=31">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=31'
			},
		"IA": {
			name: 'Iowa',
		   	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=16">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=16'
			},
		"MN": {
			name: 'Minnesota',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=28">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=28'
			},
		"OK": {
			name: 'Oklahoma',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=42">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=42'
			},
		"TX": {
			name: 'Texas',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=51">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=51'
			},
		"NM": {
			name: 'New Mexico',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=38">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=38'
			},
		"KS": {
			name: 'Kansas',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=20">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=20'
			
			},
		"NE": {
			name: 'Nebraska',
		    //description: 'default',
			description: '<p><a class="pre" href="StateProfile.aspx?StateID=35">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=35'
		
			},
		"SD": {
			name: 'South Dakota',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=49">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=49'
			},
		"ND": {
			name: 'North Dakota',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=34">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=34'
			},
		"WY": {
			name: 'Wyoming',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=59">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=59'
			},
		"MT": {
			name: 'Montana',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=32">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=32'
			},
		"CO": {
			name: 'Colorado',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=7">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=7'
			},
		"UT": {
			name: 'Utah',
		    	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=52">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=52'
			},
		"AZ": {
			name: 'Arizona',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=5">State Profile</a></p>',
		    color: 'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=5'
			},
		"NV": {
			name: 'Nevada',
		   	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=39">State Profile</a></p>',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=39'
			},
		"OR": {
			name: 'Oregon',
		   	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=43">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=43'
			},
		"WA": {
			name: 'Washington',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=56">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=56'
			},
		"CA": {
			name: 'California',
		   	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=6">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=6'
			},
		"MI": {
			name: 'Michigan',
		    description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=27">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=27'
			},
		"ID": {
			name: 'Idaho',
		   	description: 'default',
			//description: '<p><a class="pre" href="StateProfile.aspx?StateID=17">State Profile</a></p>',

			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=17'
		},

		"PR": {
		    name: 'Puerto Rico',
		    description: 'default',
		    color: 'default',
		    hover_color: 'default',
		    url: 'StateProfile.aspx?StateID=45',
		    hide: 'no'
		   
		},
		// Territories - Hidden unless hide is set to "no"
		"GU": {
			name: 'Guam',
			//description: 'default',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=',
			hide: 'yes'
			},
		"VI": {
			name: 'Virgin Islands',
			image_source: 'x.png',			
			//description: 'default',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=',
			hide: 'yes'
			},
		
		"MP": {
			name: 'Northern Mariana Islands',
			description: 'default',
			color:  'default',
			hover_color: 'default',
			 url: 'StateProfile.aspx?StateID=',
			hide: 'yes'
		}
		
        
		},
	
	//locations:{
		
	//	0: {
	//	    name: 'Bureau of Indian Education',
	//		lat: '25.2180556',
	//		lng: '-99.9002778', 
	//		url: 'StateProfile.aspx?StateID=60',
	//		color: '#999900',
	//		type: 'square',
	//	    opacity: '1'
	//	}
	//},
	
	labels:{
		"HI": {
		    color: '#999900',
			hover_color: 'default',			
			font_family: 'default',
			pill: 'yes',	
			width: 'default15',
		},
		"PR": {
		    color: '#999900',
		    hover_color: 'default',
		    font_family: 'default',
		    pill: 'yes',
		    width: 'default15'
		},		

		//'0': { parent_type: 'location', parent_id: '0', line: 'yes', name: 'BIE',  type: 'circle', location_size: '20' },
	}
	
}




