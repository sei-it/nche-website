﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" EnableEventValidation = "false" CodeFile="StateProfile.aspx.cs" Inherits="StateProfile" %>



<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="FeaturedContent" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
                <asp:Repeater ID="Repeater2" runat="server">
                    <ItemTemplate>
                        <h1><%# Eval("StateName")%></h1>
                    </ItemTemplate>
                </asp:Repeater>
                <p>
                    <i>All data presented here are drawn from the most recently available years of the U.S. Department of Education's EDFacts
                      Initiative unless otherwise noted. Click <a href="http://www2.ed.gov/about/inits/ed/edfacts/index.html" target="_blank">here</a> to learn more about the EDFacts Initiative.</i>
                </p>
            </div>
        </div>
    </div>
    <div class="grid grid-pad">
        <div class="col-5-12">
            <div class="content">
                <div class="content">
                    <h2>STATE CONTACT</h2>

                    <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                            <h3><%# Eval("Combined name")%></h3>
                            <p><%# Eval("Title")%></p>
                            <p><%# Eval("Organization")%></p>


                            <p><%# Eval("Address1")%></p>

                            <p><%# Eval("Address2")%></p>


                            <p><%# Eval("City")%>, <%# Eval("State")%> <%# Eval("Zip")%></p>

                            <p>Phone: <%# Eval("Phone Number")%></p>

                            <p>Fax: <%# Eval("Fax Number")%></p>
                            <p>Email: <%# Eval("Email")%></p>
                            <p>
                                Website: <a href="<%# Eval("Website") %>" target="_blank" style="text-decoration: none"><span style="color: blue"><%# Eval("StateName")%> Department of Education EHCY Program</span></a>
                            </p>

                        </ItemTemplate>
                    </asp:Repeater>

                    <div id="divhide" runat="server">
                        <asp:Repeater ID="RLiason" runat="server">
                            <ItemTemplate>
                                <a href="<%# Eval("LiaisonDirectory") %>" target="_blank" style="text-decoration: none"><span style="color: blue"><%# Eval("StateName")%> Liaison Directory </span></a>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="content">
                    <div id="divContact" runat="server" visible="false" class="content">
                        <br />
                        <asp:Repeater ID="Repeater11" runat="server">
                            <ItemTemplate>
                                <h3><%# Eval("Combined name2")%></h3>

                                <p><%# Eval("Organization22")%></p>
                                <p><%# Eval("Organization23")%></p>

                                <p><%# Eval("Address12")%></p>

                                <p><%# Eval("Address22")%></p>


                                <p><%# Eval("City2")%>, <%# Eval("State2")%> <%# Eval("Zip2")%></p>

                                <p>Phone: <%# Eval("Phone Number2")%></p>

                                <p>Fax: <%# Eval("Fax Number2")%></p>
                                <p>Email: <%# Eval("Email2")%></p>

                            </ItemTemplate>
                        </asp:Repeater>

                        <div id="div1" runat="server">
                            <asp:Repeater ID="Repeater12" runat="server">
                                <ItemTemplate>
                                    <a href="<%# Eval("LiaisonDirectory") %>" target="_blank" style="text-decoration: none"><span style="color: blue"><%# Eval("StateName")%> Liaison Directory </span></a>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
            <br />
            <div class="content">
                <h2>FAST FACTS</h2>
                <br />
                <div class="col-9-12 fastfacts fastfacts2">
                    <div class="content">
                        <asp:Repeater ID="Repeater7" runat="server">
                            <ItemTemplate>
                                <p class="ffnonea">Total number of Local Education Agencies (LEAs) in this state: </p>
                                <p class="ffindenta">Number of LEAs receiving McKinney‐Vento subgrants: </p>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater ID="Repeater8" runat="server">
                            <ItemTemplate>
                                <p class="ffnoneb">Total students enrolled in LEAs: </p>
                                <p class="ffindentb">Percentage of enrolled students who are homeless: </p>
                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater ID="Repeater9" runat="server">
                            <ItemTemplate>
                                <p class="ffnonec">Percentage of all people in the state who are below the poverty level <sup>1</sup> : </p>
                                <p class="ffindentc">Percentage of people under 18 years old in the state who are below the poverty level <sup>1</sup> : </p>
                            </ItemTemplate>
                        </asp:Repeater>


                    </div>
                </div>
                <div class="col-2-12 fastfacts fastfacts3">
                    <div class="content">

                        <asp:Repeater ID="Repeater5" runat="server">
                            <ItemTemplate>
                                <p class="ffnonea"><%# string.Format("{0:N0}", Eval("LEA_Tot"))%></p>
                                <p class="ffnoneb"><%# string.Format("{0:N0}", Eval("LEA_Grant"))%></p>
                            </ItemTemplate>
                        </asp:Repeater>
                        <asp:Repeater ID="Repeater6" runat="server">
                            <ItemTemplate>
                                <p class="ffnonec"><%# string.Format("{0:N0}", Eval("MEMBER"))%></p>
                                <p class="ffnoned"><%# Eval("Perc_Hmls_Enr_1213")%> %</p>

                            </ItemTemplate>
                        </asp:Repeater>

                        <asp:Repeater ID="Repeater3" runat="server">
                            <ItemTemplate>
                                <p class="ffnonee"><%# Eval("Perc_below_pov")%>%</p>

                                <p class="ffnonef"><%# Eval("Perc_kids_pov")%>%</p>

                            </ItemTemplate>
                        </asp:Repeater>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-7-12">
            <div class="content">
                <h2>Number of Homeless Children/Youth Enrolled in Public School by Year</h2>
                <br />
                <div id="container" style="min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto"></div>
                <asp:Label ID="Notes" Text="Note: Includes all enrolled homeless children and youth in grades PK through 12." runat="server"></asp:Label>
            </div>

            <div class="content">

                <h2>Subgroups of Homeless Children/Youth</h2>
                <p>Number of homeless children/youth enrolled in public schools who are:</p>

                <asp:GridView ID="GridView1" runat="server" AllowPaging="false" AutoGenerateColumns="false" Width="500px"
                    DataKeyNames="StateName" Height="155px">
                    <Columns>

                        <asp:BoundField DataField="SubGroupType" HeaderText="Subgroup Type" ItemStyle-Width="250px" DataFormatString="{0:g}">
                            <HeaderStyle Width="200px" />
                        </asp:BoundField>

                        <asp:BoundField DataField="Totle1213" HeaderText="SY 2012-2013" ItemStyle-Width="100px" DataFormatString="{0:N0}">
                            <HeaderStyle Width="100px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Totle1314" HeaderText="SY 2013-2014" ItemStyle-Width="100px" DataFormatString="{0:N0}">
                            <HeaderStyle Width="100px" />
                        </asp:BoundField>

                    </Columns>
                </asp:GridView>
                <p style="margin-top: -30px">
                    Note: These subgroups are not mutually exclusive. It is possible for homeless students to be
                   counted in more than one subgroup.
                </p>
            </div>
        </div>
    </div>
    <p class="leftpadd"><sup>1</sup> Source: US Census Bureau, Current Population<br />
        Survey, 2014 Annual Social and Economic Supplement</p>
    <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
                <h2>Percentage of homeless children/youth enrolled in public schools by type of primary nighttime residence</h2>
            </div>
        </div>
        <div class="grid grid-pad">
            <div class="col-1-3 mobile-col-1-3">
                <div class="content">
                    <%--<h3>SY 2011‐2012</h3>--%>
                    <div id="PieChart12"></div>
                </div>
            </div>
            <div class="col-1-3 mobile-col-1-3">
                <div class="content">
                    <%-- <h3>SY 2012‐2013</h3>--%>

                    <%-- <div id="PieChart13" style="min-width: 360px; max-width:650px; height: 450px; margin: 0 auto"></div>--%>
                    <div id="PieChart13"></div>
                </div>
            </div>
            <div class="col-1-3 mobile-col-1-3">
                <div class="content">
                    <%--<h3>SY 2013‐2014</h3>--%>

                    <div id="PieChart14"></div>
                </div>
            </div>
        </div>
        <div>
            <p>
                <img src="Images/black.png" style="padding-left: 250px" />
                Shelters, transitional housing,awaiting foster care
            </p>
            <p>
                <img src="Images/green.png" style="padding-left: 250px" />
                Doubled‐up (e.g., living with another family)
            </p>
            <p>
                <img src="Images/lightBlue.png" style="padding-left: 250px" />
                Unsheltered (e.g., cars, parks,campgrounds, temporary trailer, or abandoned building)
            </p>
            <p>
                <img src="Images/blue.png" style="padding-left: 250px" />
                Hotels/Motels
            </p>

        </div>
    </div>
    <div class="grid grid-pad">
        <div class="col-1-1">
            <div class="content">
                <h2>Additional State Data Notes:</h2>
                <div style="padding-left: 25px">
                    <asp:Repeater ID="Repeater4" runat="server">
                        <ItemTemplate>
                            <p><%# Eval("Comment 1")%></p>
                            <p><%# Eval("Comment 2")%></p>
                        </ItemTemplate>
                    </asp:Repeater>
                    <asp:Label ID="lblAddnotes" runat="server" ForeColor="Red"></asp:Label>
                    <asp:TextBox ID="AddStateNotes" runat="server" Height="50px" Width="700px" Visible="false" TextMode="MultiLine"></asp:TextBox>
                </div>
                <h2>Summary Sheets and Additional Data:</h2>
                <ul>

                    <li>Download data from Section 1.9 of the Consolidated State Performance Reports for the state using the drop‐down box.
                        <p>
                           <asp:DropDownList ID="ddlConsolidatedSPR" OnSelectedIndexChanged="ddlConsolidatedSPR_SelectedIndexChanged" runat="server" AutoPostBack="true"></asp:DropDownList>                         
                           
                            <asp:Button ID="btndownload" runat="server" Text="Download" OnClick="btndownload_Click" />

                        </p>
                        <p>
                            <asp:Label ID="LiteralMessage" runat="server" ForeColor="Red"></asp:Label></p>
                        <p>
                            Note: Before SY2010‐2011, reading and mathematics assessment data were only collected for students served in LEAs with
                            McKinney‐Vento subgrants. As of SY2010‐2011, these data are now collected for all students enrolled in all LEAs.
                            Enrollment figures prior to SY 2010‐2011 are not comparable to those after and including SY 2010‐2011.<br />
                            Note: Figures in these Summary Sheets are based on LEA‐reported data and contain duplicates. These figures may not match the
                            figures presented here.
                        </p>
                    </li>
                    <li style="display: none">Download a .PDF report of the data presented on this page.
                        <asp:Button ID="btnConvertToPDF" runat="server" Text="Download to PDF" OnClick="btnConvertToPDF_Click" />
                    </li>

                    <li>Download a spreadsheet of McKinney‐Vento Education for Homeless Children and Youth Actual State Funding Allocations by year for all states.
                            <%--<a href="http://nchespp.serve.org/download/412?state_id=7" target="_blank"><button>Click me !</button> Download</a>--%>
                       <%--  <asp:HyperLink ID="btnSpreadsheets" Text='DOWNLOAD' class="HyperLink"  NavigateUrl='http://nchespp.serve.org/download/412?state_id=7'    runat="server" />--%>
                        
                        <asp:Button ID="btnSpreadsheets" runat="server" Visible="true" Text="Download" OnClick="btnSpreadsheets_Click" />
                    </li>
                    <li>Click <a href="http://www2.ed.gov/about/overview/budget/statetables/index.html" target="_blank"><span style="color: blue">here </span></a> to go to the U.S. Department of Education's webpage for State Tables of agency program fund allocations.</li>
                </ul>
            </div>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="Scripts/highcharts.js"></script>


    <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" >--%>
    <%-- <script src="Scripts/jquery-1.7.1.min.js"></script>--%>



    <script lang="javascript">



        $(document).ready(function () {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                // url: "http://localhost:61912/Services/Ws.asmx/SEAEnrolled_Analysis",
                url: "Services/Wsbar.asmx/SEAEnrolled_Analysis",
                data: "{'StateName':'<%=StateNameMap%>'}",
                dataType: "json",
                success: function (Result) {

                    Result = Result.d;
                    var data = [];
                    for (var i in Result) {
                        var serie = new Array(Result[i].Name, Result[i].Value);

                        data.push(serie);
                    }
                    DreawChartBar(data);
                },
                error: function (Result) {
                    alert("Error");
                }
            });
        });

        var yAxis = {
            'Thousands': function () {
                var yAxis = {
                    title: {
                        text: 'Children/Youth in (Thousands)'
                    }

                };
                return yAxis;
            }
        };

        function DreawChartBar(series) {
            $('#container').highcharts({
                // colors: ['#2f7ed8', '#910000', '#8bbc21'],
                //  colors: ['#999900', '#003366', '#333333'],

                colors: ['#999900', '#003366', '#333333'],
                chart: {
                    type: 'bar'
                },
                title: {
                    text: ''
                },
                subtitle: {
                    text: ''
                },
                xAxis: {
                    categories: ['SY 2011 - 2012', 'SY 2012 - 2013', 'SY 2013 - 2014'],
                    title: {
                        text: null
                    }
                },
                //yAxis: {
                //    min: 0,
                //    tickInterval: 20000,
                //    max: 300000,
                //    title: {
                //        text: 'Children/Youth in (Thousands)',
                //        align: 'high'
                //    },
                //    labels: {
                //        overflow: 'justify'
                //    }
                //},
                yAxis: yAxis.Thousands(),


                tooltip: {
                    // pointFormat: "Value: {point.Value}"
                    // pointFormat: '{point.name}</b>'                 
                    enabled: false
                    // formatter: function () { return  Highcharts.numberFormat(this.y, 0, '', ',')  }
                    // pointFormat: '{this.y:,.0f}'
                },

                //tooltip: {
                //    formatter: function () {
                //        var s = Highcharts.numberFormat(this.y, 0, '', ',');
                //            return s;
                //    }
                //},

                plotOptions: {
                    bar: {
                        cursor: 'pointer',
                        colorByPoint: true,
                        dataLabels: {
                            enabled: true,
                            distance: 0,

                        }
                    }
                },

                legend: {
                    enabled: false
                },
                credits: {
                    enabled: false
                },
                //series: [{
                //    name: '',
                //    data: [17000, 29000, 18790]

                series: [{
                    type: 'bar',
                    name: '',
                    data: series,
                    dataLabels: {
                        //formatter: function () { return container.numberFormat(this.Value, 0, '', ','); },
                        formatter: function () {
                            var s = Highcharts.numberFormat(this.y, 0, '', ',');
                            return s;
                        }
                    }


                }]

            });
        }

        //----------- scripts and function for "Pie chart"------------//
        $(document).ready(function () {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                // url: "http://localhost:61912/Services/Ws.asmx/SEAEnrolled_Analysis",
                url: "Services/Ws.asmx/SEAEnrolled_Analysis",
                data: "{'StateName':'<%=StateNameMap%>', SchoolYear : '2011-2012' }",
                dataType: "json",
                success: function (Result) {

                    Result = Result.d;
                    var data = [];
                    for (var i in Result) {
                        var serie = new Array(Result[i].Name, Result[i].Value);
                        data.push(serie);
                    }
                    DreawChart(data);
                },
                error: function (Result) {
                    alert("Error");
                }
            });
        });


        function DreawChart(series) {

            $('#PieChart12').highcharts({
                colors: ['#999900', '#003366', '#333333', '#336699'],
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 1, //null,
                    plotShadow: false,

                },
                title: {
                    text: 'SY 2011-2012'
                },
                //tooltip: {
                //    pointFormat: '<br/>{point.percentage:.1f}%</b>'
                //},

                tooltip: {
                    userHTML: true,
                    style: {
                        padding: 10,
                        width: 250,
                        height: 60,
                    },
                    pointFormat: '<br/>{point.percentage:.1f}%</b>'
                   
                },

                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 0,
                            format: '{point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '',
                    data: series
                }]
            });
        }


        $(document).ready(function () {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                // url: "http://localhost:61912/Services/Ws.asmx/SEAEnrolled_Analysis",
                url: "Services/Ws.asmx/SEAEnrolled_Analysis",
                data: "{'StateName':'<%=StateNameMap%>', SchoolYear : '2012-2013' }",
                dataType: "json",
                success: function (Result) {

                    Result = Result.d;
                    var data = [];
                    for (var i in Result) {
                        var serie = new Array(Result[i].Name, Result[i].Value);
                        data.push(serie);
                    }
                    DreawChart2(data);
                },
                error: function (Result) {
                    alert("Error");
                }
            });
        });
        function DreawChart2(series) {

            $('#PieChart13').highcharts({
                colors: ['#999900', '#003366', '#333333', '#336699'],

                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 1, //null,
                    plotShadow: false
                },
                title: {
                    text: 'SY 2012-2013'
                },

                //tooltip: {
                //    pointFormat: '{point.percentage:.1f}%'
                //},

                tooltip: {
                    userHTML: true,
                    style: {
                        padding: 10,
                        width: 250,
                        height: 60,
                    },
                    pointFormat: '<br/>{point.percentage:.1f}%</b>'

                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 0,
                            format: '{point.percentage:.1f} %',
                            // format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '',
                    data: series
                }]
            });
        }

        $(document).ready(function () {

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                // url: "http://localhost:61912/Services/Ws.asmx/SEAEnrolled_Analysis",
                url: "Services/Ws.asmx/SEAEnrolled_Analysis",
                data: "{'StateName':'<%=StateNameMap%>', SchoolYear : '2013-2014' }",
                dataType: "json",
                success: function (Result) {

                    Result = Result.d;
                    var data = [];
                    for (var i in Result) {
                        var serie = new Array(Result[i].Name, Result[i].Value);
                        data.push(serie);
                    }
                    DreawChart3(data);
                },
                error: function (Result) {
                    alert("Error");
                }
            });
        });
        function DreawChart3(series) {

            $('#PieChart14').highcharts({
                colors: ['#999900', '#003366', '#333333', '#336699'],
                chart: {
                    plotBackgroundColor: null,
                    plotBorderWidth: 1, //null,
                    plotShadow: false
                },
                title: {
                    text: 'SY 2013-2014'
                },
                //tooltip: {
                //    pointFormat: '{point.percentage:.1f}%'
                //},

                tooltip: {
                    userHTML: true,
                    style: {
                        padding: 10,
                        width: 250,
                        height: 60,
                    },
                    pointFormat: '<br/>{point.percentage:.1f}%</b>'

                },
                credits: {
                    enabled: false
                },
                plotOptions: {
                    pie: {
                        allowPointSelect: false,
                        cursor: 'pointer',
                        dataLabels: {
                            enabled: true,
                            distance: 0,
                            format: '{point.percentage:.1f} %',
                            //format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                            style: {
                                color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                            }
                        }
                    }
                },
                series: [{
                    type: 'pie',
                    name: '',
                    data: series
                }]
            });
        }
    </script>

</asp:Content>

